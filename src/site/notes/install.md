## 포트 구성

### JEUS7 기준 포트 구성
![[Pasted image 20220825153754.png]]


### AnyLink7 기준 포트 구성

![[Pasted image 20220825153810.png]]

---

## 사전준비

애니링크 설치를 위한 Installer / Patch 올려두기

→ Anylink Installer 파일은 Technet에서 다운로드 가능

Technet > 다운로드 > 인터페이스 프레임워크 > Anylink7 
![[Pasted image 20220825153843.png]]


→ 패치 파일은 QA FTP 서버 다운로드 가능

![[Pasted image 20220825153923.png]]
---

## 서버 구성

### JDK 설치

```bash
# 1. 다운받은 JDK 압축 파일을 원하는 위치에 압축 해제

# 2. alternative 사용해서 java 환경변수 설정
[root@tmaxsoft binary]# alternatives --install /usr/bin/java java /usr/lib/jvm/jdk1.8.0_311/bin/java 100
[root@tmaxsoft binary]# alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk1.8.0_311/bin/javac 100
[root@tmaxsoft binary]# alternatives --config java

There are 2 programs which provide 'java'.

  Selection    Command
-----------------------------------------------
*+ 1           java-1.8.0-openjdk.x86_64 (/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.312.b07-1.el7_9.x86_64/jre/bin/java)
   2           /usr/lib/jvm/jdk1.8.0_311/bin/java

Enter to keep the current selection[+], or type selection number: 2
[root@tmaxsoft anylink]# alternatives --config javac

There is 1 program that provides 'javac'.

  Selection    Command
-----------------------------------------------
*+ 1           /usr/lib/jvm/jdk1.8.0_311/bin/javac

Enter to keep the current selection[+], or type selection number: 1

# 3. 변경사항 확인
[root@tmaxsoft anylink]# java -version
java version "1.8.0_311"
Java(TM) SE Runtime Environment (build 1.8.0_311-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.311-b11, mixed mode)

[root@tmaxsoft anylink]# javac -version
javac 1.8.0_311
```

### 방화벽 설정

```bash
# 방화벽이 막혀있는 경우 접속 불가 (8808 9736 7730 7127)
# 설정한 포트는 vi /etc/firewalld/zones/public.xml 확인 가능
[root@anylink ~]# firewall-cmd --permanent --zone=public --add-port=8808/tcp
success

# 수정사항 적용
[root@anylink ~]# firewall-cmd --reload
success

-----------------------------------------------------------

# 방화벽 완전 내리기
[root@anylink ~]# systemctl stop firewalld

# 재부팅시 방화벽 실행 X
[root@anylink ~]# systemctl disable firewalld

# 방화벽 허용 목록 보기
[root@anylink ~]# firewall-cmd --list-ports
```

### hostname 변경시

```bash
# 1
[root@anylink ~]$ hostnamectl set-hostname [사용할 호스트명]

# 2
[root@anylink ~]$ vi /etc/hostname
localhost.localdomain 삭제 후 사용할 호스트명 입력

# 3
[root@anylink ~]$ vi /etc/hosts
192.168.56.115 toif1
{IP}           {hostname}
```

---

## Database 설치

### Tibero

```bash
# 환경설정 
--------------------------------------------------
[root@anylink ~]$ vi /etc/sysctl.conf

	kernel.sem = 10000 32000 10000 10000
	kernel.shmall = 524288
	# ceil(shmmax/PAGE_SIZE)
	# getconf PAGESIZE
	kernel.shmmax = 2147483648
	kernel.shmmni = 4096
	
	fs.file-max = 6815744
	net.ipv4.ip_local_port_range = 1024 65500
--------------------------------------------------
[root@anylink ~]$ vi /etc/security/limits.conf

	tibero           soft    nproc           2047
	tibero           hard    nproc           16384
	tibero           soft    nofile          1024
	tibero           hard    nofile          65536
--------------------------------------------------

# tibero 계정 추가
[root@anylink home]# useradd -d /home/tibero -g dba -p tibero -s /bin/bash -m -k /etc/skel tibero
# tibero 계정 비밀번호 설정
[root@anylink home]# passwd tibero
# 권한 부여
[root@anylink home]# chmod -R 755 /home/tibero
[root@anylink home]# chown -R tibero:dba /home/tibero

# tibero 계정에 binary 생성 -> tibero tar.gz 가져와서 압축 해제 
[tibero@anylink binary]$ tar -xvf tibero6-bin-FS07_CS_2005-linux64-186930-opt-tested.tar.gz

# /home/tibero/tibero6/license/license.xml 데모 라이센스 넣어주기

# /home/tibero/tibero6/config/gen_tip.sh 파일 실행해주기
[tibero@anylink config]$ gen_tip.sh
Using TB_SID "tibero"
/home/tibero/tibero6/config/tibero.tip generated
/home/tibero/tibero6/config/psm_commands generated
/home/tibero/tibero6/client/config/tbdsn.tbr generated.
Running client/config/gen_esql_cfg.sh
Done.
--------------------------------------------------------

# /home/tibero/tibero6/config/tibero.tip 수정
--------------------------------------------------------
	############################
	###Base Info.
	############################
	DB_NAME=tibero
	LISTENER_PORT=8629
	CONTROL_FILES=/home/tibero/tbdata/control01.ctl
	DB_CREATE_FILE_DEST=/home/tibero/tbdata
	#CONTROL_FILES="/home/tibero/tibero6/database/tibero/c1.ctl"
	#CERTIFICATE_FILE="/home/tibero/tibero6/config/tb_wallet/tibero.crt"
	#PRIVKEY_FILE="/home/tibero/tibero6/config/tb_wallet/tibero.key"
	#WALLET_FILE="/home/tibero/tibero6/config/tb_wallet/WALLET"
	#ILOG_MAP="/home/tibero/tibero6/config/ilog.map"
	
	MAX_SESSION_COUNT=20
	
	#############################
	###TSM(Tibero Shared Memory)
	#############################
	TOTAL_SHM_SIZE=2G
	MEMORY_TARGET=4G
--------------------------------------------------------

# vi /home/tibero/tibero6/client/config/tbdsn.tbr DB계정 정보 설정
--------------------------------------------------------
	tibero=(
	    (INSTANCE=(HOST=192.168.56.113)
	              (PORT=8629)
	              (DB_NAME=tibero)
	    )
	)
--------------------------------------------------------

# tbinary경로 생성
[tibero@anylink tibero6]$ mkdir tbinary
[tibero@anylink tibero6]$ cd tbinary/
[tibero@anylink tbinary]$ mkdir info dat sql

# tibero 실행 및 database 생성
[tibero@anylink ~]$ pwd
/home/tibero
[tibero@anylink ~]$ tbboot nomount
Warning: The initialization parameter 'MEMORY_TARGET' value is greater than the physical system memory size. MEMORY_TARGET: 4294967296 Physical System Memory Size: 3973521408Excessive memory consumption may cause the system to slow down or malfunction. It is recommended to set the MEMORY_TARGET to a value less than the physical system memory size.
Change core dump dir to /home/tibero/tibero6/bin/prof.
Listener port = 8629

Tibero 6

TmaxData Corporation Copyright (c) 2008-. All rights reserved.
Tibero instance started up (NOMOUNT mode).
[tibero@anylink ~]$ tbsql sys/tibero

tbSQL 6

TmaxData Corporation Copyright (c) 2008-. All rights reserved.

Connected to Tibero.

SQL> create database "tibero" user sys identified by tibero maxinstances 8 maxdatafiles 100 character set MSWIN949 national character set UTF16 logfile group 1 'log001.log' size 100M, group 2 'log002.log' size 100M, group 3 'log003.log' size 100M maxloggroups 255 maxlogmembers 8 noarchivelog datafile 'system001.dtf' size 100M autoextend on next 100M maxsize unlimited default temporary tablespace TEMP tempfile 'temp001.dtf' size 100M autoextend on next 100M maxsize unlimited extent management local autoallocate undo tablespace UNDO datafile 'undo001.dtf' size 100M autoextend on next 100M maxsize unlimited extent management local autoallocate;

# -> database 생성하면 /home/tibero/tbdata 폴더가 생성되어 있다.

# $TB_HOME/scripts/system.sh 실행 전부 Y

# shell 실행이 완료되면 ps –ef | grep tbsvr 사용해서 프로세스 정상 기동 확인                                                   
```

### MariaDB

```bash
# 10.3으로 진행 / 10.4 안되었음

# yum repository 설정
## 다운그레이드 & 업그레이드 필요한 경우 yum 으로 설치된 패키지들을 모두 지우고 repo 설정을 바꾼 뒤 재설치 하면 설정값으로 다운 가능
[root@tmaxsoft ~]$ sudo vi /etc/yum.repos.d/MariaDB.repo
	---------------------------------------------------------------
	# MariaDB 10.3 CentOS repository list - created 2019-01-13 00:47 UTC
	# <http://downloads.mariadb.org/mariadb/repositories/>
	[mariadb]
	name = MariaDB
	baseurl = <http://yum.mariadb.org/10.3/centos7-amd64>
	gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
	gpgcheck=1
	---------------------------------------------------------------

# MARIA DB 설치
[root@tmaxsoft ~]$ sudo yum install MariaDB-server MariaDB-client

# MARIA DB 서버 시작
# MariaDB 부팅 시 자동 시작 설정
[root@tmaxsoft ~]$ sudo systemctl enable mariadb

# MariaDB 시작 명령어
[root@tmaxsoft ~]$ sudo systemctl start mariadb
[root@tmaxsoft ~]$ sudo service mariadb start

# 정상적으로 실행되는지 확인
[root@tmaxsoft ~]$ netstat -an | grep 3306
tcp6       0      0 :::3306                 :::*                    LISTEN
[root@tmaxsoft ~]$ sudo service mariadb status
...

# root 비밀번호 설정
## 설정하면 sudo mysql / use mysql 대신 mysql -u root -p mysql 로 접속 가능
MariaDB [(none)]> use mysql
MariaDB [mysql]> update user set plugin='' where User='root';
MariaDB [mysql]> set password = password('root');
MariaDB [mysql]> flush privileges;
```

-   참고
    
    ```bash
    # Database 목록 보기
    > show databases;
    
    # Database 생성하기
    > create database [DB 이름];
    
    # Database 삭제하기
    > drop database [DB이름];
    
    # 사용할 Database 선택
    > use [DB이름];
    
    # 사용자 계정 조회
    > select user, host from mysql.user;
    
    # 사용자 계정 생성
    > create user 'userid'@'host' identified by 'password';
    # -> userid와 password는 원하는 값으로 설정.
    # -> host는 접속 클라이언트의 주소.('%'를 지정하면 anywhere)
    
    # 사용자 계정 권한 부여
    > grant all privileges on [DB이름].[Table이름] to 'userid'@'host';
    # -> all은 insert, select, delete, update 등으로 바꿀 수 있다. 여러 개 지정시 콤마로 분리하여 한 꺼번에 지정하면된다.
    #    [DB이름].[Table이름]은 wildcard(*)를 사용할 수 있다. (*, *.*, db_name.*, ... )
    #    보안상의 이유로 일반 사용자 계정은 [DB이름].* 이상의 권한을 주지 않도록 한다.
    
    # 사용자 권한 확인
    >  show grants for 'userid'@'host';
    
    # 사용자 권한 삭제
    > revoke all on [DB이름].[Table이름] from 'userid'@'host';
    
    # 사용자 계정 삭제
    > drop user 'userid'@'host';
    
    # 권한 변경사항 적용
    > flush privileges;
    ```
    
    ### ERROR 1045 (28000) → mysql 로그인 시
    
    ```bash
    # 로그인 실패 에러
    [anylink@anylink ~]$ mysql -u anylink -p anylink
    Enter password:
    ERROR 1045 (28000): Access denied for user 'anylink'@'localhost' (using password: YES) 
    # 라고 에러가 발생하는 경우 YES이면 비밀번호가 틀린 것
    # root 계정으로 접속해 해당 계정의 비밀번호를 업데이트
    
    MariaDB [mysql]> update user set password=password('anylink') where user='anylink';
    Query OK, 1 row affected (0.000 sec)
    Rows matched: 1  Changed: 1  Warnings: 0
    
    MariaDB [mysql]> flush privileges;
    Query OK, 0 rows affected (0.000 sec)
    
    # database 부재 에러 
    [anylink@anylink ~]$ mysql -u anylink -p anylink
    Enter password:
    ERROR 1044 (42000): Access denied for user 'anylink'@'localhost' to database 'anylink'
    # -> anylink 데이터베이스가 없기 때문에 발생함 root로 생성해주고 재접속 하기 
    
    # anylink DB 생성 
    MariaDB [(none)]> CREATE database anylink default CHARACTER SET UTF8;
    Query OK, 1 row affected (0.000 sec)
    
    # DB 목록 조회
    MariaDB [(none)]> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | anylink            |
    | information_schema |
    | mysql              |
    | performance_schema |
    | test               |
    +--------------------+
    5 rows in set (0.001 sec)
    ```
    
    ## SQLInvalidAuthorizationSpecException Error
    
    ```bash
    # dsboot 중... 
    <<**Exception**>>
    javax.naming.NamingException: java.sql.SQLInvalidAuthorizationSpecException: Could not connect to address=(host=192.168.56.115)(port=3306)(type=master) : (conn=9) Access denied for user 'anylink'@'toif1' (using password: YES)
    ...
    Caused by: java.sql.SQLInvalidAuthorizationSpecException: Could not connect to address=(host=192.168.56.115)(port=3306)(type=master) : (conn=9) Access denied for user 'anylink'@'toif1' (using password: YES)
    
    # 계정 인증 문제... 
    # mysql 계정 host를 toif1(hostname)로 새로 생성해주니 연결 성공함
    MariaDB [(mysql)]> grant all privileges on *.* to anylink@'toif1' identified by 'anylink';
    MariaDB [(mysql)]> create user 'anylink'@'toif1' identified by 'anylink';
    
    ```
    

---

## AnyLink 설치

### AnyLink 설치

```bash
[anylink@anylink binary]$ ./AnyLink_unix_ko.bin

===============================================================================
Choose Platform # -> 설치 환경에 따른 플랫폼 선택
---------------

Choose the operating system and architecture :
1)HP-UX PA-RISC
2)HP-UX Itanium
3)Solaris UltraSPARC
4)Solaris x86
5)Solaris x64
6)AIX 5.x, 6.x, 7.x PowerPC 
7)Linux Itanium
8)Linux x86
9)Linux x64 # -> Centos7 / Ubuntu
10)Linux PowerPC 64
Quit) Quit Installer

Choose Current System (DEFAULT: 9):
===============================================================================
Installation Folder # -> anylink 설치 폴더 경로 지정 : /home/anylink/anylink7
-------------------

Enter the installation folder.

  Default Install Folder: /home/anylink/jeus7

ENTER AN ABSOLUTE PATH, OR PRESS <ENTER> TO ACCEPT THE DEFAULT :
	# /home/anylink/anylink7
===============================================================================
Installation Mode # -> 설치 모드 지정, 보통 Production Mode 사용
-----------------

* Production Mode
- Disables JEUS Hot Swap.
- Disables Automatic Reloading.
- Displays a warning message and recommends using a full license if a demo
license is used.

* Development Mode
- Enables JEUS Hot Swap.
- Enables Automatic Reloading.

  ->1- Production Mode
    2- Development Mode
    3- Cancel

ENTER THE NUMBER OF THE DESIRED CHOICE, OR PRESS <ENTER> TO ACCEPT THE
   DEFAULT:
===============================================================================
Enter the JDK path. # -> JDK가 설치되어 있는 경로 지정
-------------------

Enter the JDK path:

Enter the JDK path (DEFAULT: /usr/local/jdk-1.8):
===============================================================================
Password Input # -> JEUS의 administrator 계정 비밀번호 설정
-------------- #    Password:: anylink7

Enter the Password for the administrator account.

Input Password:: 

Corfirm Password::
===============================================================================
User Domain name # -> JEUS의 domain name 설정
----------------

Enter the Domain name

Enter the domain name (DEFAULT: anylink_domain):
===============================================================================
Get User LocalHost IP # -> 해당 서버 IP를 입력한다
---------------------

IP (DEFAULT: 192.168.56.100):
===============================================================================
Get User Variable # -> DIS 접속 포트를 지정한다 PORT : 7127
-----------------

DIS Port (DEFAULT: 7127):
===============================================================================
Choose DBMS type # -> Anylink Repository DBMS 선택
----------------
  ->1- Tibero
    2- Oracle

ENTER THE NUMBER FOR YOUR CHOICE, OR PRESS <ENTER> TO ACCEPT THE DEFAULT:
===============================================================================
Get Tibero JDBC Source # -> 연결할 DBMS 정보를 입력한다
----------------------

Enter requested information

Tibero DB NAME (DEFAULT: tibero):

Tibero Port (DEFAULT: 8629):

Tibero DB Server IP (DEFAULT: 192.168.56.113):

Tibero User ID (DEFAULT: anylink):

Tibero PassWord (DEFAULT: anylink):
====================================================================
Pre-Installation Summary # -> install 정보 요약
------------------------

Review the Following Before Continuing:

Product Name:
    AnyLink

Install Folder:
    /home/anylink/anylink7

Link Folder:
    /home/anylink

Install Set
    Domain Admin Server

Disk Space Information (for Installation Target):
    Required:  1,648,838,448 bytes
    Available: 29,923,139,584 bytes

PRESS <ENTER> TO CONTINUE:
====================================================================

```

### AnyLink ERD 스크립트

-   Tibero
    
    ```bash
    # tibero 실행 후 anylink 계정 생성 및 권한 부여
    SQL> create user anylink identified by anylink;
    User 'ANYLINK' created.
    SQL> grant dba to anylink;
    Granted.
    SQL> commit;     
    
    # DB script 실행
    ## create : AnyLink 스키마 생성
    ## insert : 기본 데이터 삽입
    SQL> @create-anylink-dis-ERD6.0.70.sql
    SQL> @insert-anylink-dis-ERD6.0.70.sql
    ```
    
-   MariaDB
    
    ```bash
    # anylink 사용자 추가 및 권한 부여
    # 방법 1
    MariaDB [mysql]> create user 'anylink'@'%' identified by 'anylink';
    MariaDB [mysql]> grant all privileges on *.* to anylink@'%';
    # 방법 2
    MariaDB [mysql]> grant all privileges on *.* to tmax@'hostname' identified by 'tmax';
    MariaDB [mysql]> flush privileges;
    
    # mysql 사용자 및 DB 조회
    MariaDB [mysql]> select user, host from user;
    MariaDB [mysql]> show databases;
    
    # anylink DB 생성 
    MariaDB [(none)]> CREATE database anylink default CHARACTER SET UTF8;
    
    # anylink DB 접속
    MariaDB [(none)]> use anylink
    
    # DB script 실행
    ## create : AnyLink 스키마 생성
    ## insert : 기본 데이터 삽입
    MariaDB [anylink]> source create-anylink-MARIA-ERD6.0.70.sql
    MariaDB [anylink]> source insert-anylink-MARIA-ERD6.0.70.sql
    ```
    

### AnyLink 패치

-   Anylink 최초 설치 기준
    
-   JEUS 패치
    
    ```bash
    # /home/anylink/anylink7/lib/jext에 있는 jext 파일을 전부 지우고 패치 할 jext 파일들로 바꿔준다
    [anylink@anylink jext]$ rm -rf *
    
    # 패치할 파일 Stability_Binary_v20210524/jext 에서 옮기기
    [anylink@anylink jext_fix3_20210524]$ cp -rp * /home/anylink/anylink7/lib/jext/
    ```
    
-   3rd Party 라이브러리 패치
    
    ```bash
    # 기존 freemarker 파일 삭제
    [anylink@anylink application]$ rm /home/anylink/anylink7/domains/anylink_domain/lib/application/freemarker-2.3.8.jar
    
    # 패치 바이너리 복사
    [anylink@anylink lib]$ cp freemarker-2.3.8-tmax.jar /app/anylink/domains/oanylink_domain/lib/application/
    ```
    
-   AnyLink 엔진 바이너리 패치
    
    ```bash
    # 기존 경로에 존재하던 바이너리 삭제
    [anylink@anylink application]$ rm -rf /home/anylink/anylink7domains/anylink_domain/lib/application/anylink-distribution-*.jar
    
    # 패치 바이너리 복사
    [anylink@anylink Stability_Binary_v20210524]$ cp anylink-distribution-*.jar /home/anylink/anylink7/domains/anylink_domain/lib/application/
    ```
    
-   AnyLink 웹어드민 패치
    
    ```bash
    # 기존 anylink-admin.war 삭제 
    [anylink@anylink ~]$ cd anylink7/domains/anylink_domain/.applications/anylink-admin/
    [anylink@anylink anylink-admin]$ ll
    total 14884
    -rwxr-xr-x. 1 anylink anylink 15240576 Dec 17 10:16 anylink-admin.war
    
    [anylink@anylink anylink-admin]$ rm -rf anylink-admin.war
    
    # 패치 바이너리 복사 시 파일명 변경 - anylink-admin.war
    [anylink@anylintability_Binary_v20210524]$ cp anylink-admin-dev-jeus7-210521_1301.war /home/anylink/anylink7/domains/anylink_domain/.applications/anylink-admin/anylink-admin.war
    ```
    

### profile - AnyLink7 Wiki 참고하여 진행

[[profile]]

### 기동/중지 스크립트 - AnyLink7 Wiki 참고하여 진행

[기동/중지 Script](https://www.notion.so/Script-d88917aebbfd433b956602638db9ea3a)

### JEUS 설정 - AnyLink7 Wiki 참고하여 진행

[JEUS 설정](https://www.notion.so/JEUS-cdaf39db767f4da2b86b60b8170ffebc)

### AnyLink 설정 - AnyLink7 Wiki 참고하여 진행

[AnyLink 설정](https://www.notion.so/AnyLink-9de4c15f9488420dab1b6a78c0480c25)

### HTTP Application

\[JEUS Webadmin]-\[Applications] > Deployed Application

![[Pasted image 20220825154354.png]]
-   [anylink-http-server]은 HTTP 통신을 위해 사용되는 Application으로 MS(or Cluster)가 추가될 때마다 [add-target]을 통해 추가 필요
    -   adminServer는 추가할 필요 없음

---

## 클러스터

### Node 생성

-   MS2를 담당할 노드 등록(NodeManager 접속 정보 기준)
    
    [JEUS Webadmin]-[Node 설정]-[ADD+]
    ![[Pasted image 20220825154206.png]]
      
-   MS2 에서 NodeManager 기동 후 **Under Control** 상태가 **Y**로 표시됨을 확인
    ![[Pasted image 20220825154231.png]]

    

### MS 복사

-   \[JEUS Webadmin\]-\[Servers\]
    
     ![[Pasted image 20220825155413.png]]
       

   ![[Pasted image 20220825155405.png]]
toifContainer1의 [DUP+] 클릭 화면
    
-   Name : MS2사용 할 MS명
-   Node Name : 2번서버에 만든 Node Name을 클릭
-   Listen Address : 2번서버의 IP
-   Listen Port : Base Port로 사용 할 Port

### bizsystem.config 수정

-   (2번 서버) msboot 전 bizsystem.config 수정 작업 필요
    
    초기 설치 시 존재하는 domain/anylink_domain/servers/server1을 활용해 MS를 추가할 때마다 bizsystem.config 경로와 내용을 참고해 작성
    
-   bizsystem.config 내에서 MS명에 해당하는 엘리먼트값(nodeList > node > name)을 수정해야 함
    
    ```bash
    # 1. 설치 시 생성 된 server1 디렉토리를 추가 한 MS 명으로 복사
    ## $ANYLINK_HOME/domains/$DOMAIN_NAME/servers/$MS_NAME/repository/bizsystem/bizsystem.config
    [anylink@anylink servers]$ cp -rp server1 $MS_NAME
    
    # 2. 추가 된 MS명의 bizsystem.config 수정
    [anylink@anylink servers]$ vi /home/anylink/anylink7/domains/anylink_domain/servers/$MS_NAME/repository/bizsystem
    ...
    <ns0:bizSystemDefaultThreadPoolId>bizSystemDefaultThreadPool</ns0:bizSystemDefaultThreadPoolId>
    	<ns0:systemLogging>
    		<ns0:logLevel>FINEST</ns0:logLevel>
    	</ns0:systemLogging> 
    	<ns0:nodeList> 
    		<ns0:node> 
    			<ns0:name>$MS_NAME</ns0:name> <-- 추가한 MS명으로 수정해주기
    		</ns0:node> 
    	</ns0:nodeList>
    ...
    ```
    
-   bizsystem.config 수정 후 (2번 서버) 기동
→ MS2 가 성공적으로 부팅 되었다면 RUNNING 상태로 나타남
    ![[Pasted image 20220825155459.png]]
   


### MS2 환경

```bash
# 1. DIS 관련 삭제
rm -rf $ANYLINK_HOME/dis_config
rm -rf $ANYLINK_HOME/server

# 2. adminServer 삭제
rm -rf $ANYLINK_HOME/domains/$DOMAIN_NAME/servers/adminServer
```

### Lifecycle Invocation 삭제

\[JEUS Webadmin]-\[servers]-\[$MS]-\[resource]-\[lifecycle]

-   클러스터로 구성하는 경우 각 MS에 존재하는 Lifecycle Invocation class를 삭제해준다
    
    
    

### 클러스터 생성

\[JEUS Webadmin]-\[Clusters]

-   Servers
    ![[Pasted image 20220825155847.png]]   

### HTTP Application

\[JEUS Webadmin]-\[Applications]

-   anylink-http-servlet 의 타겟으로 기존에 존재하던 별개의 MS를 삭제하고 클러스터를 지정해준다
    
    ![[Pasted image 20220825155824.png]]
    

### 클러스터 업무시스템

-   업무시스템 생성 시 `**[구성종류]-[서버 클러스터]**`
    
    ![[Pasted image 20220825155811.png]]
    
    -   나머지 설정은 AnyLink 설정과 동일하다
