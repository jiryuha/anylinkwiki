# 디렉토리 구성

-   **# : 경로 수동 생성 필요 / 버전에 따라 다름**
-   $ANYLINK_HOME
    -   dis_config
        -   dis_config.xml
    -   domains
        -   $DOMAIN_NAME
            -   config
                -   domain.xml
            -   servers
                -   $MS_NAME**(#)**
                    -   repository
                        -   bizsystem
                            -   bizsystem.config
    -   nodemanager
        -   nodemanager.properties
-   $LOG_HOME
    -   JEUS_LOG**(#)**
        -   nodemanager**(#)**
        -   launcher**(#)**
        -   adminServer
        -   $MS_NAME
        -   gclog**(#)**
        -   heapdump**(#)**
    -   ANYLINK_LOG**(#)**
        -   dis
        -   repository_log
        -   rte_$MS_NAME
        -   txlog**(#)**
            -   $MS_NAME**(#)**

# Profile

## DAS + MS

```bash
################################################
#   Encoding SETTING
#   locale -a을 통해 인코딩 확인 후 설정
################################################
export LANG=ko_KR.utf8

################################################
#   JAVA SETTING
################################################
export JAVA_HOME=/usr/lib/jvm/java-1.8.0
export PATH=$JAVA_HOME/bin:$PATH

################################################
#   ANYLINK SETTING
################################################
export JEUS_HOME=/home/anylink/anylink7
export PATH="$JEUS_HOME/bin:$JEUS_HOME/lib/system:$JEUS_HOME/webserver/bin:${PATH}"
export ANYLINK_HOME=/home/anylink/anylink7
export LOG_HOME=$ANYLINK_HOME/logs
export JLOG_HOME=$LOG_HOME/JEUS_LOG
export ALOG_HOME=$LOG_HOME/ANYLINK_LOG

# Jeus Info
export JEUS_ID=administrator
export JEUS_PWD=anylink7

# DAS(DomainAdminServer) Info
export DOMAIN_NAME=anylink_domain
export DAS_NAME=adminServer
export DAS_HOST=192.168.56.101
export DAS_PORT=9736

# NM(NodeManager) Info
export NM_PORT=7730

# MS(ManagedServer) Info
export MS1_NAME=server1
export MS1_PORT=9936
#export MS2_NAME=server2
#export MS2_PORT=9946
#export MS3_NAME=server3
#export MS3_PORT=9956

################################################
#   ALIAS SETTING
################################################
alias pp='ps -ef | grep java | grep `whoami`'
alias vdm='vi $ANYLINK_HOME/domains/$DOMAIN_NAME/config/domain.xml'
alias vnm='vi $ANYLINK_HOME/nodemanager/jeusnm.properties'

alias dsa='jeusadmin -host $DAS_HOST -port $DAS_PORT -u $JEUS_ID -p $JEUS_PWD'
alias msa1='jeusadmin -host $DAS_HOST -port $MS1_PORT -u $JEUS_ID -p $JEUS_PWD'
#alias msa2='jeusadmin -host $DAS_HOST -port $MS2_PORT -u $JEUS_ID -p $JEUS_PWD'
#alias msa3='jeusadmin -host $DAS_HOST -port $MS3_PORT -u $JEUS_ID -p $JEUS_PWD'

alias ah='cd $ANYLINK_HOME'
alias jbin='cd $ANYLINK_HOME/bin'
alias jcfg='cd $ANYLINK_HOME/domains/$DOMAIN_NAME/config'
alias dcfg='cd $ANYLINK_HOME/dis_config'
alias svr='cd $ANYLINK_HOME/domains/$DOMAIN_NAME/servers'

alias logs='cd $LOG_HOME'
alias jlog='cd $JLOG_HOME'
alias alog='cd $ALOG_HOME'
alias dlog='cd $ALOG_HOME/dis'

alias jlogd='cd $JLOG_HOME/adminServer'
alias jlog1='cd $JLOG_HOME/$MS1_NAME'
#alias jlog2='cd $JLOG_HOME/$MS2_NAME'
#alias jlog3='cd $JLOG_HOME/$MS3_NAME'

alias alog1='cd $ALOG_HOME/rte_$MS1_NAME'
#alias alog2='cd $ALOG_HOME/rte_$MS2_NAME'
#alias alog3='cd $ALOG_HOME/rte_$MS3_NAME'

alias dtail='tail -f $ALOG_HOME/dis/dis_`date +%Y%m%d`.log'
alias atail1='tail -f $ALOG_HOME/rte_$MS1_NAME/anylink_`date +%Y%m%d`.log'
#alias atail2='tail -f $ALOG_HOME/rte_$MS2_NAME/anylink_`date +%Y%m%d`.log'
#alias atail3='tail -f $ALOG_HOME/rte_$MS3_NAME/anylink_`date +%Y%m%d`.log'

alias jtaild='tail -f $JLOG_HOME/adminServer/JeusServer.log'
alias jtail1='tail -f $JLOG_HOME/$MS1_NAME/JeusServer.log'
#alias jtail2='tail -f $JLOG_HOME/$MS2_NAME/JeusServer.log'
#alias jtail3='tail -f $JLOG_HOME/$MS3_NAME/JeusServer.log'
```

## MS

```bash
################################################
#   Encoding SETTING
#   locale -a을 통해 인코딩 확인 필 후 설정
################################################
export LANG=ko_KR.utf8

################################################
#   JAVA SETTING
################################################
export JAVA_HOME=/usr/java/jdk1.7.0_80
export PATH=$JAVA_HOME/bin:$PATH

################################################
#   ANYLINK SETTING
################################################
export JEUS_HOME=/home/anylink/anylink7
export PATH="$JEUS_HOME/bin:$JEUS_HOME/lib/system:$JEUS_HOME/webserver/bin:${PATH}"
export ANYLINK_HOME=/home/anylink/anylink7
export LOG_HOME=$ANYLINK_HOME/logs
export JLOG_HOME=$LOG_HOME/JEUS_LOG
export ALOG_HOME=$LOG_HOME/ANYLINK_LOG

# Jeus Info
export JEUS_ID=administrator
export JEUS_PWD=anylink7

# DAS(DomainAdminServer) Info
export DOMAIN_NAME=anylink_domain
export DAS_NAME=adminServer
export DAS_HOST=192.168.56.101
export DAS_PORT=9736

export ANYLINK_IP=`hostname`

# NM(NodeManager) Info
export NM_PORT=7730

# MS(ManagedServer) Info
export MS1_NAME=server1
export MS1_PORT=9936
#export MS2_NAME=server2
#export MS2_PORT=9946
#export MS3_NAME=server3
#export MS3_PORT=9956

################################################
#   ALIAS SETTING
################################################
alias pp='ps -ef | grep java | grep `whoami`'
alias vdm='vi $ANYLINK_HOME/domains/$DOMAIN_NAME/config/domain.xml'
alias vnm='vi $ANYLINK_HOME/nodemanager/jeusnm.properties'

alias msa1='jeusadmin -host $ANYLINK_IP -port $MS1_PORT -u $JEUS_ID -p $JEUS_PWD'
#alias msa2='jeusadmin -host $ANYLINK_IP -port $MS2_PORT -u $JEUS_ID -p $JEUS_PWD'
#alias msa3='jeusadmin -host $ANYLINK_IP -port $MS3_PORT -u $JEUS_ID -p $JEUS_PWD'

alias ah='cd $ANYLINK_HOME'
alias jbin='cd $ANYLINK_HOME/bin'
alias jcfg='cd $ANYLINK_HOME/domains/$DOMAIN_NAME/config'
alias svr='cd $ANYLINK_HOME/domains/$DOMAIN_NAME/servers'

alias logs='cd $LOG_HOME'
alias jlog='cd $JLOG_HOME'
alias alog='cd $ALOG_HOME'

alias jlog1='cd $JLOG_HOME/$MS1_NAME'
#alias jlog2='cd $JLOG_HOME/$MS2_NAME'
#alias jlog3='cd $JLOG_HOME/$MS3_NAME'

alias alog1='cd $ALOG_HOME/rte_$MS1_NAME'
#alias alog2='cd $ALOG_HOME/rte_$MS2_NAME'
#alias alog3='cd $ALOG_HOME/rte_$MS3_NAME'

alias atail1='tail -f $ALOG_HOME/rte_$MS1_NAME/anylink_`date +%Y%m%d`.log'
#alias atail2='tail -f $ALOG_HOME/rte_$MS2_NAME/anylink_`date +%Y%m%d`.log'
#alias atail3='tail -f $ALOG_HOME/rte_$MS3_NAME/anylink_`date +%Y%m%d`.log'

alias jtail1='tail -f $JLOG_HOME/$MS1_NAME/JeusServer.log'
#alias jtail2='tail -f $JLOG_HOME/$MS2_NAME/JeusServer.log'
#alias jtail3='tail -f $JLOG_HOME/$MS3_NAME/JeusServer.log'
```